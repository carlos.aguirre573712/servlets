package modelos;

public class Almacen {
    private int codProducto;
    private String fechaIngreso;
    private String fechaDevolucion;
    private String categoria;

    public Almacen(int cod_Producto, String fecha_ingreso, String fecha_Devolucion, String categoria) {
        this.codProducto = cod_Producto;
        this.fechaIngreso = fecha_ingreso;
        this.fechaDevolucion = fecha_Devolucion;
        this.categoria = categoria;
    }

    public Almacen() {
    }

    public int getCod_Producto() {
        return codProducto;
    }

    public void setCod_Producto(int cod_Producto) {
        this.codProducto = cod_Producto;
    }

    public String getFecha_ingreso() {
        return fechaIngreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fechaIngreso = fecha_ingreso;
    }

    public String getFecha_Devolucion() {
        return fechaDevolucion;
    }

    public void setFecha_Devolucion(String fecha_Devolucion) {
        this.fechaDevolucion = fecha_Devolucion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
