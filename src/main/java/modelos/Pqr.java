package modelos;

public class Pqr {

    private int idPqr;
    private String fechaDevolucion;
    private String fechaCreacion;
    private String observacion;
    private String contenido;
    private int idUsuario;
    private String estado;
    private String tipoPqr;
    private int idGarantia;

    public Pqr() {
    }

    public Pqr(int idPqr, String fechaDevolucion, String fechaCreacion, String observacion, String contenido, int idUsuario, String estado, String tipoPqr, int idGarantia) {
        this.idPqr = idPqr;
        this.fechaDevolucion = fechaDevolucion;
        this.fechaCreacion = fechaCreacion;
        this.observacion = observacion;
        this.contenido = contenido;
        this.idUsuario = idUsuario;
        this.estado = estado;
        this.tipoPqr = tipoPqr;
        this.idGarantia = idGarantia;
    }

    public int getIdPqr() {
        return idPqr;
    }

    public String getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(String fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTipoPqr() {
        return tipoPqr;
    }

    public void setTipoPqr(String tipoPqr) {
        this.tipoPqr = tipoPqr;
    }

    public int getIdGarantia() {
        return idGarantia;
    }

    public void setIdGarantia(int idGarantia) {
        this.idGarantia = idGarantia;
    }
}
