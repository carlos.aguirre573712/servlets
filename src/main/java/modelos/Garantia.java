package modelos;

public class Garantia {
    private int idGarantia;
    private int codProducto;
    private int idUsuario;

    public Garantia() {
    }

    public Garantia(int idGarantia, int codProducto, int idUsuario) {
        this.idGarantia = idGarantia;
        this.codProducto = codProducto;
        this.idUsuario = idUsuario;
    }

    public int getIdGarantia() {
        return idGarantia;
    }

    public int getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(int codProducto) {
        this.codProducto = codProducto;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}
