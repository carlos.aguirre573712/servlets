package modelos;

public class Respuesta {
    private int idRespuesta;
    private int idPqr;
    private  String respuesta;

    public Respuesta() {
    }

    public Respuesta(int idRespuesta, int idPqr, String respuesta) {
        this.idRespuesta = idRespuesta;
        this.idPqr = idPqr;
        this.respuesta = respuesta;
    }

    public int getIdRespuesta() {
        return idRespuesta;
    }

    public int getIdPqr() {
        return idPqr;
    }

    public void setIdPqr(int idPqr) {
        this.idPqr = idPqr;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
