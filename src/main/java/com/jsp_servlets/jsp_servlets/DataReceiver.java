package com.jsp_servlets.jsp_servlets;

import java.io.*;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.ServerException;

@WebServlet(name = "dataReceiver", value = "/DataReceiver")
public class DataReceiver extends HttpServlet {
    private void processsRequest(HttpServletRequest request, HttpServletResponse response, String msg)
    throws ServerException, IOException {

        response.setContentType("text/html; charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            // Hello
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Receiver</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Receiver at" + msg + "</h1>");
            out.println("</body></html>");
        }
    }
@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String name = request.getParameter("nombres");
        String lastName = request.getParameter("apellidos");
        String rol = request.getParameter("rol");
        String cel = request.getParameter("celular");
        String mail = request.getParameter("email");
        String cc = request.getParameter("cedula");
        String pass = request.getParameter("password");

        if ("admin".equals(name) && "1234".equals(pass)) {

            processsRequest(request, response, "Registro exitoso");
        } else {
            processsRequest(request, response, "Registro invalido");
        }
        // response.setContentType("text/html");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // response.setContentType("text/html");
    }
    public void destroy() {
    }

}
