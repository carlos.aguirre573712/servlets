package com.jsp_servlets.jsp_servlets;

import controladores.UsuarioCtrl;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.*;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import modelos.Usuarios;

import java.io.IOException;
import java.rmi.ServerException;

@WebServlet(name = "ServletCtrl", value = "/ServletCtrl")
public class ServletCtrl extends HttpServlet {

    String listar= "views/listar.jsp";
    String edit= "views/editar.jsp";
    String editUsr= "views/editarUsr.jsp";
    String add= "views/add.jsp";
    String addUsr= "views/addUsr.jsp";
    Usuarios user = new Usuarios();
    UsuarioCtrl usrCtrl = new UsuarioCtrl();
    private void processsRequest(HttpServletRequest request, HttpServletResponse response, String msg)
            throws ServerException, IOException {
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String access="";
        String action= request.getParameter("accion");
        if(action.equalsIgnoreCase("listar")){
            access= listar;
        } else if (action.equalsIgnoreCase("addUsr")){
            access= addUsr;
        } else if (action.equalsIgnoreCase("Agregar")) {
            String nombres = request.getParameter("txtNombres");
            String apellidos = request.getParameter("txtApellidos");
            int rol = Integer.parseInt(request.getParameter("intRol"));
            String celular = request.getParameter("txtCelular");
            String email = request.getParameter("txtEmail");
            String cedula = request.getParameter("txtCedula");
            String password = request.getParameter("txtPassword");

            user.setNombres(nombres);
            user.setApellidos(apellidos);
            user.setRol(rol);
            user.setCelular(celular);
            user.setEmail(email);
            user.setCedula(cedula);
            user.setPassword(password);

            usrCtrl.add(user);
            access= listar;
        } else if (action.equalsIgnoreCase("editarUsr")) {
            request.setAttribute("idUsr", request.getParameter("id"));
            access= editUsr;
        } else if (action.equalsIgnoreCase("Actualizar")) {
            int id = Integer.parseInt(request.getParameter("intId"));
            String nombres = request.getParameter("txtNombres");
            String apellidos = request.getParameter("txtApellidos");
            int rol = Integer.parseInt(request.getParameter("intRol"));
            String celular = request.getParameter("txtCelular");
            String email = request.getParameter("txtEmail");
            String cedula = request.getParameter("txtCedula");
            String password = request.getParameter("txtPassword");

            user.setId(id);
            user.setNombres(nombres);
            user.setApellidos(apellidos);
            user.setRol(rol);
            user.setCelular(celular);
            user.setEmail(email);
            user.setCedula(cedula);
            user.setPassword(password);

            usrCtrl.edit(user);
            access=listar;
        } else if (action.equalsIgnoreCase("eliminar")) {
            int id= Integer.parseInt(request.getParameter("id"));
            user.setId(id);
            usrCtrl.delete(id);
            access=listar;
        }

        RequestDispatcher view = request.getRequestDispatcher(access);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}