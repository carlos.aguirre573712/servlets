package repositorios;

import modelos.Usuarios;

import java.sql.SQLException;
import java.util.List;

public interface Crud {
    public List listar() throws SQLException;
    public Usuarios list(int id);
    public boolean add(Usuarios usr);
    public boolean edit(Usuarios usr);
    public boolean delete(int id);
}
