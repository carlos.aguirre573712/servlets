package controladores;

import repositorios.Crud;
import config.ConnectionJdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import modelos.Usuarios;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static config.ConnectionJdbc.obtenerConexion;

public class UsuarioCtrl implements Crud {

    //ConnectionJdbc cn = new ConnectionJdbc();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Usuarios usuario = new Usuarios();


    @Override
    public List listar() throws SQLException {
        //Connection conexion = ConnectionJdbc.obtenerConexion();

        String sql = "SELECT * FROM usuario";
        ArrayList<Usuarios> list = new ArrayList<>();
        try {
            con = ConnectionJdbc.obtenerConexion();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Usuarios user = new Usuarios();
                user.setId(rs.getInt("id_Usuario"));
                user.setNombres(rs.getString("Nombres"));
                user.setApellidos(rs.getString("Apellidos"));
                user.setRol(rs.getInt("Rol"));
                user.setCelular(rs.getString("Celular"));
                user.setEmail(rs.getString("Email"));
                user.setCedula(rs.getString("Cedula"));
                list.add(user);
            }
        }catch (Exception e){}

        return list;
    }

    @Override
    public Usuarios list(int id) {

        String sql = "SELECT * FROM usuario WHERE id_Usuario="+id;

        try {
            con = ConnectionJdbc.obtenerConexion();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                usuario.setId(rs.getInt("id_Usuario"));
                usuario.setNombres(rs.getString("Nombres"));
                usuario.setApellidos(rs.getString("Apellidos"));
                usuario.setRol(rs.getInt("Rol"));
                usuario.setCelular(rs.getString("Celular"));
                usuario.setEmail(rs.getString("Email"));
                usuario.setCedula(rs.getString("Cedula"));
                usuario.setPassword(rs.getString("Password"));
            }
        }catch (Exception e){
            throw new RuntimeException(e);
        }
        return usuario;
    }

    @Override
    public boolean add(Usuarios usr) {

        String sql = "INSERT INTO usuario (nombres, apellidos, rol, celular, email, cedula, password) VALUES ('"+usr.getNombres()+"','"+usr.getApellidos()+"', '"+usr.getRol()+"', '"+usr.getCelular()+"', '"+usr.getEmail()+"', '"+usr.getCedula()+"', '"+usr.getPassword()+"')";

        try {
            con = ConnectionJdbc.obtenerConexion();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return false;
    }

    @Override
    public boolean edit(Usuarios usr) {
        String sql = "UPDATE usuario SET Nombres=?, Apellidos=?, Rol=?, Celular=?, Email=?, Cedula=?, Password=? WHERE id_Usuario=?";
        try {
            con = ConnectionJdbc.obtenerConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, usr.getNombres());
            ps.setString(2, usr.getApellidos());
            ps.setInt(3, usr.getRol());
            ps.setString(4, usr.getCelular());
            ps.setString(5, usr.getEmail());
            ps.setString(6, usr.getCedula());
            ps.setString(7, usr.getPassword());
            ps.setInt(8, usr.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        String sql="DELETE FROM usuario WHERE id_Usuario="+id;
        try {
            con = ConnectionJdbc.obtenerConexion();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
}


/*

    public void create() {


        if (cn != null) {
            String query = "INSERT INTO usuario (nombres, apellidos, rol, celular, email, cedula, password) VALUES (?, ?, ?, ?, ?, ?, ?)";

            try (PreparedStatement preparedStatement = cn.prepareStatement(query)) {
                preparedStatement.setString(1, this.nombres);
                preparedStatement.setString(2, this.apellidos);
                preparedStatement.setString(3, this.rol);
                preparedStatement.setString(4, this.celular);
                preparedStatement.setString(5, this.email);
                preparedStatement.setString(6, this.cedula);
                preparedStatement.setString(7, this.password);

                preparedStatement.executeUpdate();
                System.out.println("Usuario insertado correctamente.");
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Error al insertar el usuario en la base de datos.");
            } finally {
                Conexion.cerrarConexion(conexion);
            }
        }
    }



    public void getAllUsers() throws SQLException {
        Connection conexion = Conexion.obtenerConexion();

        if (conexion != null) {
            Statement statement = conexion.createStatement();
            ResultSet resultSet;


            resultSet = statement.executeQuery("SELECT * FROM usuario");
            resultSet.next();
            do {
                System.out.println(resultSet.getInt("id_Usuario") + ": "+ resultSet.getString("Nombres"));
            }while (resultSet.next());

            // Cierra la conexión cuando hayas terminado.
            Conexion.cerrarConexion(conexion);

        }

    }*/
