package config;

import java.sql.*;

public class ConnectionJdbc {
    private static final String URL = "jdbc:mysql://localhost:3306/CCO_db";
    private static final String USUARIO = "root";
    private static final String CONTRASENA = "123456*.*";

    public static Connection obtenerConexion() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection(URL, USUARIO, CONTRASENA);
        } catch (ClassNotFoundException e) {
            throw new SQLException("Error: No se pudo cargar el controlador de MySQL.", e);
        }
    }

    public static void cerrarConexion(Connection conexion) {
        if (conexion != null) {
            try {
                conexion.close();
            } catch (SQLException e) {
                System.err.println("Error al cerrar la conexión a la base de datos.");
                e.printStackTrace();
            }
        }
    }
}
