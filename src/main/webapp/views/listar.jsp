<%@ page import="controladores.UsuarioCtrl" %>
<%@ page import="java.util.List" %>
<%@ page import="modelos.Usuarios" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.sql.SQLException" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Usuarios</title>
</head>
<body>
<div>
    <h1>Usuarios</h1>
    <a href="ServletCtrl?accion=addUsr">Agregar Usuario</a>
    <br><br>
    <table border="1">
        <thead>
        <tr>
            <th>ID</th>
            <th>NOMBRES</th>
            <th>APELLIDOS</th>
            <th>ROL</th>
            <th>CELULAR</th>
            <th>EMAIL</th>
            <th>CEDULA</th>
            <th>ACCIONES</th>
        </tr>
        </thead>
        <%
            UsuarioCtrl uCtrl = new UsuarioCtrl();
            List<Usuarios> list = uCtrl.listar();
            System.out.println(list);
            Iterator<Usuarios> iter = list.iterator();
            Usuarios user = null;

            while (iter.hasNext()) {
                user = iter.next();
        %>
        <tbody>
        <tr>
            <td><%= user.getId() %></td>
            <td><%= user.getNombres() %></td>
            <td><%= user.getApellidos() %></td>
            <td><%= user.getRol() %></td>
            <td><%= user.getCelular() %></td>
            <td><%= user.getEmail() %></td>
            <td><%= user.getCedula() %></td>
            <td>
                <a href="ServletCtrl?accion=editarUsr&id=<%= user.getId()%>">Editar</a>
                <a href="ServletCtrl?accion=eliminar&id=<%= user.getId()%>">Remove</a>
            </td>
        </tr>
        <%}%>
        </tbody>

    </table>
</div>
</body>
</html>
