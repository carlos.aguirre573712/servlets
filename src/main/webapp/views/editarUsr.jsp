<%@ page import="controladores.UsuarioCtrl" %>
<%@ page import="modelos.Usuarios" %><%--
  Created by IntelliJ IDEA.
  User: black
  Date: 14/11/2023
  Time: 9:34 p. m.
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Modificar usuario</title>
</head>
<body>
<%
    UsuarioCtrl usrCtrl= new UsuarioCtrl();
    int id=Integer.parseInt((String)request.getAttribute("idUsr"));
    Usuarios user = usrCtrl.list(id);
%>
<h1>Modificar usuario</h1>
<form action="ServletCtrl">
    Nombres: <br>
    <input type="text" name="txtNombres" value="<%=user.getNombres()%>"><br>
    Apellidos:<br>
    <input type="text" name="txtApellidos" value="<%=user.getApellidos()%>"><br>
    Rol:<br>
    <input type="text" name="intRol" value="<%=user.getRol()%>"><br>
    Celular:<br>
    <input type="text" name="txtCelular" value="<%=user.getCelular()%>"><br>
    Email:<br>
    <input type="text" name="txtEmail" value="<%=user.getEmail()%>"><br>
    Cedula:<br>
    <input type="text" name="txtCedula" value="<%=user.getCedula()%>"><br>
    Password:<br>
    <input type="text" name="txtPassword" value="**********"><br>
    <br>
    <input type="hidden" name="intId" value="<%=user.getId()%>"><br>
    <input type="submit" name="accion" value="Actualizar"><br>
    <a href="ServletCtrl?accion=listar">Regresar</a>

</form>
</body>
</html>
