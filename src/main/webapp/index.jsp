<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
  <title>JSP - Hello World</title>
</head>
<body>
<div>
<h1><%= "Ingreso de usuario" %></h1>
<br/><br/>
<form name="insertar" action="DataReceiver" method="get">
  <br/>
  <!-- Campo Nombres -->
  <label for="nombres">Nombres:</label>
  <input type="text" id="nombres" name="nombres" required placeholder="Ingrese sus nombres">
  <br/><br/>
  <!-- Campo Apellidos -->
  <label for="apellidos">Apellidos:</label>
  <input type="text" id="apellidos" name="apellidos" required placeholder="Ingrese sus apellidos">
  <br/><br/>
  <!-- Campo Rol -->
  <label for="rol">Rol:</label>
  <input type="text" id="rol" name="rol" required placeholder="Ingrese su rol">
  <br/><br/>
  <!-- Campo Celular -->
  <label for="celular">Celular:</label>
  <input type="tel" id="celular" name="celular" required placeholder="Ingrese su número de celular">
  <br/><br/>
  <!-- Campo Email -->
  <label for="email">Email:</label>
  <input type="email" id="email" name="email" required placeholder="Ingrese su dirección de correo electrónico">
  <br/><br/>
  <!-- Campo Cedula -->
  <label for="cedula">Cédula:</label>
  <input type="text" id="cedula" name="cedula" required placeholder="Ingrese su número de cédula">
  <br/><br/>
  <!-- Campo Password -->
  <label for="password">Contraseña:</label>
  <input type="password" id="password" name="password" required placeholder="Ingrese su contraseña">
  <br/><br/>
  <!-- Botón de envío -->
  <button type="submit">Enviar</button>
</form>
</div>
<div>
  <a href="ServletCtrl?accion=listar">Listar usuarios</a>
</div>
</body>
</html>